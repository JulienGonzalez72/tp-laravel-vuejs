<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountController extends Controller {

  private $users = [
    ['id' => 1,'name' => 'user1', 'email' => 'user1@domaine.com', 'password' => 'user1pwd'],
    ['id' => 2,'name' => 'user2', 'email' => 'user2@domaine.com', 'password' => 'user2pwd'],
    ['id' => 3,'name' => 'user3', 'email' => 'user3@domaine.com', 'password' => 'user3pwd']
  ];

  public function accounts() {
    return view('accounts', ['users' => $this->users]);
  }

  public function addAccount(Request $request) {
    $user = [
      'id' => count($this->users) + 1,
      'name' => $request->name,
      'email' => $request->mail,
      'password' => $request->password
    ];
    $this->users[] = $user;
    return view('signupInfo', $user);
  }

}

?>