<?php

namespace App\Http\Controllers;

use Error;
use Illuminate\Http\Request;

class AccountControllerREST extends Controller
{

  private $users = [
    ['id' => 1, 'name' => 'user1', 'email' => 'user1@domaine.com', 'password' => 'user1pwd'],
    ['id' => 2, 'name' => 'user2', 'email' => 'user2@domaine.com', 'password' => 'user2pwd'],
    ['id' => 3, 'name' => 'user3', 'email' => 'user3@domaine.com', 'password' => 'user3pwd']
  ];
  
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    if ($request->ajax()) {
      return $this->users;
    } else {
      return view('accounts', ['users' => $this->users]);
    }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('signup');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $user = [
      'id' => count($this->users) + 1,
      'name' => $request->name,
      'email' => $request->mail,
      'password' => $request->password
    ];
    $this->users[] = $user;
    if ($request->ajax()) {
      return $user;
    } else {
      return view('signupInfo', $user);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $deleted = false;
    foreach ($this->users as $key => $user) {
      if ($user['id'] == $id) {
        unset($this->users[$key]);
        $deleted = $id;
      }
    }
    return view('accounts', ['users' => $this->users, 'deleteState' => $deleted]);
  }
}
