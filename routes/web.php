<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

define('USE_REST', true);

Route::get('/', function () {
    return view('main');
});

if (USE_REST) {
    Route::resource('/accounts', 'AccountControllerREST');
} else {
    Route::get('/accounts', 'AccountController@accounts');
    Route::get('/accounts/create', function () {
        return view('signup');
    });
    Route::post('/accounts', 'AccountController@addAccount');
}

Route::get('/tirage', function () {
    dump($_REQUEST);
});

Route::get('/form', function () {
    return view('form');
});

Route::post('/calculer', function () {
    dump($_REQUEST);
});