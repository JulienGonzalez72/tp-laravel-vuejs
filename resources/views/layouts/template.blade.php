<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Laravel</title>
  <link rel="stylesheet" type="text/css" href="/css/app.css"/>
  <link rel="stylesheet" type="text/css" href="/css/style.css"/>
</head>
<body>
  <div class="container-fluid">
    <div class="row justify-content-center display-4 py-3">Laravel Project</div>
    {{-- <nav class="nav justify-content-center">
      <li class="nav-item">
        <a class="nav-link" href="/">Accueil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/accounts">Liste des utilisateurs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/accounts/create">Ajouter un utilisateur</a>
      </li>
    </nav> --}}
    <hr/>
    {{-- <div class="container">
      @yield('content')
    </div> --}}
    <div id="app" class="container">
      <utilisateurs/>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/vue
/dist/vue.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
  <script src="js/script.js"></script>
  <script>
    for (const link of document.querySelectorAll('.nav-link')) {
      if (window.location.pathname === link.pathname) {
        link.classList.add('active')
      } else {
        link.classList.remove('active')
      }
    }
  </script>
</body>
</html>