@extends('layouts.template')

@section('content')
@if (isset($deleteState) && $deleteState !== false)
  <div class="pb-3 text-center">
    L'utilisateur id={{ $deleteState }} a bien été supprimé !
  </div>
@endif
<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th>Nom</th>
      <th>Email</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach ($users as $user)
    <tr>
      <td>{{ $user['name'] }}</td>
      <td>{{ $user['email'] }}</td>
      <td>
        <form action="/accounts/{{ $user['id'] }}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger">Supprimer</button>
          </form>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
@endsection