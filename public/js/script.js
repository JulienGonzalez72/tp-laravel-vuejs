axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.component('utilisateurs', {
  data() {
    return {
      users: [],
      input_username: '',
      input_mail: '',
      input_password: ''
    }
  },
  template: `
    <div>
      <table class="table table-striped">
        <thead class="thead-dark">
          <tr>
            <th>Nom</th>
            <th>Email</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(user, index) in users">
            <td>{{ user.name }}</td>
            <td>{{ user.email }}</td>
            <td>
              <button class="btn btn-danger" @click="deleteUser(user.id, index)">Supprimer</button>
            </td>
          </tr>
        </tbody>
      </table>
      <div class="form-group">
        <label>
          Nom
        </label>
        <input class="form-control" type="text" v-model="input_username" placeholder="Nom"/><br/>
      </div>
      <div class="form-group">
        <label>
          Email
        </label>
        <input class="form-control" type="text" v-model="input_mail" placeholder="Email"/><br/>
      </div>
      <div class="form-group">
        <label>
          Email
        </label>
        <input class="form-control" type="password" v-model="input_password" placeholder="Mot de passe"/><br/>
      </div>
      <button class="btn btn-primary" @click="addUser">Ajouter</button>
    </div>
  `,
  mounted() {
    axios.get('accounts')
      .then(res => {
        this.users = res.data
      })
      .catch(e => {
        console.error(e)
      })
  },
  methods: {
    addUser() {
      axios.post('accounts', {
        name: this.input_username,
        mail: this.input_mail,
        password: this.input_password
      })
        .then(res => {
          this.users.push(res.data)
        })
        .catch(e => {
          console.error(e)
        })
    },
    deleteUser(id, index) {
      axios.delete('accounts/' + id)
        .then(res => {
          this.users.splice(index, 1)
        })
        .catch(e => {
          console.error(e)
        })
    }
  }
})

let app = new Vue({
  el: '#app'
})